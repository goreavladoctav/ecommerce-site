<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @vite('resources/css/app.css')
        <title>Ecommerce Site</title>
        @routes
        @vite('resources/js/app.js')
        @inertiaHead

    </head>
    <body class="bg-background overflow-y-auto">
        @inertia
    </body>
</html>
