/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./storage/framework/views/*.php",
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
    "./vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php",

  ],
  theme: {
    extend: {
      colors: {
        'pink-red' : '#F20746',
        'indigo' : '#4C0B8C',
        'estate-blue' : '#253759',
        'blue': '#E7EFF8',
        'orange1': '#F2DB94',
        'orange2': '#F29F05',
        'pink': '#D9D4D2',
        'background': 'rgb(247,247,247)',
        'white': '#fff',
        'gray': '#999',
        'gray-light' : '#D9D9D9'
      },
      fontFamily: {
        'body': ['"Open Sans",Helvetica,Arial,sans-serif']
      },

    },


  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}

