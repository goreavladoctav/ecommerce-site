<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserAccountController;
use App\Http\Controllers\RealtorProductController;
use App\Http\Controllers\AdsController;
use App\Http\Controllers\ProductImageController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



/** route for Products */
Route::resource('products', ProductController::class)->middleware('auth');

/** route for login user */
Route::get('login', [AuthController::class, 'create'])->name('login')->middleware('guest');

Route::post('login', [AuthController::class, 'store'])->name('login.store');

Route::delete('logout', [AuthController::class, 'destroy'])->name('logout');

/**route for create user  */
Route::resource('user-account', UserAccountController::class)->only('create', 'store');

/**route for ads */
Route::get('', [AdsController::class, 'index'])->name('ads');

Route::get('/ads-details/{id}', [AdsController::class, 'show']);

/** routes for realtor */

Route::prefix('realtor')
  ->name('realtor.')
  ->middleware('auth')
  ->group(function () {
    Route::resource('product', RealtorProductController::class)
    ->only(['index', 'destroy']);

});


Route::resource('products.image', ProductImageController::class)
->only(['create', 'store', 'destroy']);



