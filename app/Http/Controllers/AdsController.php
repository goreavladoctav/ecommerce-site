<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ad;


class AdsController extends Controller
{


    public function index(Request $request) {

        $filters = $request->only([
            'priceFrom', 'priceTo', 'beds', 'baths', 'areaFrom', 'areaTo'
        ]);

        $query = Ad::all();

        if($filters['priceFrom'] ?? false) {

            $query = Ad::all()->where('price', '>=', $filters['priceFrom']);
        }

        if($filters['priceTo'] ?? false) {

            $query = Ad::all()->where('price', '=<', $filters['priceTo']);
        }

        return inertia('Ads/ShowAds',
        [
            'filters' => $filters,
            'ads' => $query

        ]);
    }

    public function show(Ad $ad, string $id) {
        return inertia('Ads/AdDetails',

            [
                'ad' => Ad::findOrFail($id),
                'ads' => Ad::all()
            ]
        );
    }


}
