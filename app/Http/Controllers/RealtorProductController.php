<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Product;


class RealtorProductController extends Controller
{
    public function index (Request $request) {
        return inertia ('Realtor/Index',
            ['products' => Auth::user()->products]
        );
    }

    public function destroy(Product $product)
    {
        $product->deleteOrFail();

        return redirect('realtor/product')->with('success', 'Ad was deleted');
    }
}
