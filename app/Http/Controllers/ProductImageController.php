<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Support\Facades\Storage;

class ProductImageController extends Controller
{
    public function create(Product $product)
    {
        ($product->load(['images']));
        return inertia('Products/AdImage/CreateImage',
            [
                'product' => $product
            ]
        );
    }
    public function store(Product $product, Request $request)
    {
        if ( $request->hasFile('images') ) {
            foreach ($request->file('images') as $file) {
                $path = $file->store('images', 'public');

                $product->images()->save(new ProductImage([
                    'filename' => $path
                ]));
            }
        }

        return  redirect()->back()->with('success', 'Image uploaded');
    }

    public function destroy( $product, ProductImage $image)
    {
        Storage::disk('public')->delete($image->filename);
        $image->delete();

        return redirect()->back()->with('success', 'Image was deleted');
    }
}
